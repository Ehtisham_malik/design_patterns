package design_patterns;

	interface Person {
	    Person clone();
	}