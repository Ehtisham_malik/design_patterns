package design_patterns;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		while(true) {
			System.out.println("\n\tPress 1 to run Prototype Design Pattern.");
			System.out.println("\tPress 2 to quit program.");
			
			Scanner scanner = new Scanner(System.in);
			
			
			int choice = scanner.nextInt();
			
			
			
			
			if(choice == 1) {
				if (args.length > 0) {
		            for (String type : args) {
		                Person prototype = Factory.getPrototype(type);
		                if (prototype != null) {
		                    System.out.println(prototype);
		                }
		            }
		        } else {
		            System.out.println("Run again with arguments of command string ");
		        }
			}else if(choice == 2){
				System.out.println("Quitting Program!");
				scanner.close();
				System.out.println("Quit!");
				System.exit(0);
			}
			
		}
		
		
		
	}

}
